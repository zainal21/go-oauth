# Go Oauth

## Getting started

Example Implementation Oauth with Google.

## Dependencies

There is some dependencies that we used in this skeleton:

- [Go Fiber](https://docs.gofiber.io/) [Go Framework]
- [Viper](https://github.com/spf13/viper) [Go Configuration]
- [Cobra](https://github.com/spf13/cobra) [Go Modern CLI]
- [Logrus Logger](https://github.com/sirupsen/logrus) [Go Logger]
- [Goose Migration](https://github.com/pressly/goose) [Go Migration]
- [Gobreaker](https://github.com/sony/gobreaker) [Go Circuit Breaker]

## Requirement

- Golang version 1.21 or latest
- Database MySQL

## Usage

### Installation

install required dependencies

```bash
make install
```

### Run Service

run current service after all dependencies installed

```bash
make start
```

### API Reference

##### Google Login

```http
  GET /api/public/v1/oauth/google
```

##### Google Callback

```http
  POST /api/public/v1/oauth-google/callback
```
