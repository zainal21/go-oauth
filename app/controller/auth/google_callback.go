package auth

import (
	"context"
	"io/ioutil"
	"net/http"

	"github.com/Zainal21/go-oauth/app/appctx"
	"github.com/Zainal21/go-oauth/app/controller/contract"
	"github.com/Zainal21/go-oauth/app/helpers"
	"github.com/Zainal21/go-oauth/app/repositories"
	"github.com/Zainal21/go-oauth/app/service"
	"github.com/Zainal21/go-oauth/pkg/config"
	"github.com/Zainal21/go-oauth/pkg/oauth"
	"github.com/gofiber/fiber/v2"
)

type GoogleCallbackImpl struct {
	service           service.UserService
	personalTokenRepo repositories.PersonalTokenRepository
	cfg               *config.Config
}

// Serve implements contract.Controller.
func (s *GoogleCallbackImpl) Serve(xCtx appctx.Data) appctx.Response {
	ctx := xCtx.FiberCtx

	state := ctx.Query("state")

	if state != "randomstate" {
		return helpers.CreateErrorResponse(fiber.StatusBadRequest, "States don't Match!!", nil)
	}

	code := ctx.Query("code")

	googlecon := oauth.GoogleConfig(context.Background(), s.cfg)

	token, err := googlecon.Exchange(context.Background(), code)
	if err != nil {
		return helpers.CreateErrorResponse(fiber.StatusUnauthorized, "Code-Token Exchange Failed!!", nil)
	}

	resp, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	if err != nil {
		return helpers.CreateErrorResponse(fiber.StatusBadRequest, "User Data Fetch Failed!!", nil)
	}

	userData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return helpers.CreateErrorResponse(fiber.StatusBadRequest, "JSON Parsing Failed!!", nil)
	}

	return *appctx.NewResponse().
		WithData(string(userData)).
		WithCode(fiber.StatusOK)
}

func NewGoogleCallback(
	svc service.UserService,
	pat repositories.PersonalTokenRepository,
	cfg *config.Config,
) contract.Controller {
	return &GoogleCallbackImpl{
		service:           svc,
		personalTokenRepo: pat,
		cfg:               cfg,
	}
}
