package auth

import (
	"context"

	"github.com/Zainal21/go-oauth/app/appctx"
	"github.com/Zainal21/go-oauth/app/controller/contract"
	"github.com/Zainal21/go-oauth/app/repositories"
	"github.com/Zainal21/go-oauth/app/service"
	"github.com/Zainal21/go-oauth/pkg/config"
	"github.com/Zainal21/go-oauth/pkg/oauth"
)

type GoogleSignInImpl struct {
	service           service.UserService
	personalTokenRepo repositories.PersonalTokenRepository
	cfg               *config.Config
}

// Serve implements contract.Controller.
func (s *GoogleSignInImpl) Serve(xCtx appctx.Data) appctx.Response {
	google := oauth.GoogleConfig(context.Background(), s.cfg)
	url := google.AuthCodeURL("random")
	return *appctx.NewResponse().WithData(map[string]interface{}{
		"url": url,
	})
}

func NewGoogleSignIn(
	svc service.UserService,
	pat repositories.PersonalTokenRepository,
	cfg *config.Config,
) contract.Controller {
	return &GoogleSignInImpl{
		service:           svc,
		personalTokenRepo: pat,
		cfg:               cfg,
	}
}
