package handler

import (
	"github.com/Zainal21/go-oauth/app/appctx"
	"github.com/Zainal21/go-oauth/app/controller/contract"
	"github.com/Zainal21/go-oauth/pkg/config"
	"github.com/gofiber/fiber/v2"
)

func HttpRequest(xCtx *fiber.Ctx, svc contract.Controller, conf *config.Config) appctx.Response {
	data := appctx.Data{
		FiberCtx: xCtx,
		Cfg:      conf,
	}
	return svc.Serve(data)
}
