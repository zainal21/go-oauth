package config

type OauthConfig struct {
	GoogleClientId     string `mapstructure:"google_client_id"`
	GoogleClientSecret string `mapstructure:"google_client_secret"`
	GoogleRedirectUrl  string `mapstructure:"google_redirect_url"`
}
