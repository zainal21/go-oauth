package oauth

import (
	"context"
	"log"

	"github.com/Zainal21/go-oauth/pkg/config"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

type OauthConfig struct {
	GoogleLoginConfig oauth2.Config
	GitHubLoginConfig oauth2.Config
}

var AppConfig OauthConfig

func GoogleConfig(ctx context.Context, conf *config.Config) oauth2.Config {
	log.Println(conf.GoogleClientId)
	AppConfig.GoogleLoginConfig = oauth2.Config{
		RedirectURL:  conf.GoogleRedirectUrl,
		ClientID:     conf.GoogleClientId,
		ClientSecret: conf.GoogleClientSecret,
		Scopes: []string{"https://www.googleapis.com/auth/userinfo.email",
			"https://www.googleapis.com/auth/userinfo.profile"},
		Endpoint: google.Endpoint,
	}

	return AppConfig.GoogleLoginConfig
}
